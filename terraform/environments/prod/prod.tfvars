region        = "eu-central-1"
ami_id        = "ami-0dcc38974463cc747"
environment   = "prod"
instance_type = "t3.micro"
deploy_key    = "prod-asg-deploy"

asg_min_size         = 1
asg_max_size         = 1
asg_min_elb_capacity = 1

main_dns_name       = "vmsap.tk"
webapp_dns_name     = ""
prometheus_dns_name = "monitoring"
grafana_dns_name    = "grafana"
docker_dns_name     = "docker"
