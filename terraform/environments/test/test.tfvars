region        = "eu-central-1"
ami_id        = "ami-0dcc38974463cc747"
environment   = "test"
instance_type = "t3.micro"
deploy_key    = "test-asg-deploy"

asg_min_size         = 1
asg_max_size         = 1
asg_min_elb_capacity = 1

main_dns_name       = "vmsap.tk"
webapp_dns_name     = "test"
prometheus_dns_name = "test.monitoring"
grafana_dns_name    = "test.grafana"
docker_dns_name     = "test.docker"


