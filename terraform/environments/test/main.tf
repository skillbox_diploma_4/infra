
provider "aws" {
  region = var.region
}

data "aws_availability_zones" "vmsap_available_zones" {}

resource "aws_default_subnet" "vmsap_available_zone_1" {
  availability_zone = data.aws_availability_zones.vmsap_available_zones.names[0]
}

resource "aws_default_subnet" "vmsap_available_zone_2" {
  availability_zone = data.aws_availability_zones.vmsap_available_zones.names[1]
}

// templates user_data

data "template_file" "gitlab_runner_user_data" {
  template = file("./scripts/gtlb_runner_user_data.sh.tpl")

  vars = {
    publickey  = tls_private_key.webapp_deploy.public_key_openssh
    privatekey = tls_private_key.webapp_deploy.private_key_pem
  }
}

data "template_file" "webapp_user_data" {
  template = file("./scripts/webapp_user_data.sh.tpl")

  vars = {
    srv_env = var.environment
  }
}

data "template_file" "monitoring_user_data" {
  template = file("./scripts/monitoring_user_data.sh.tpl")

  vars = {
    srv_env = var.environment
  }
}

// ssh keys

resource "tls_private_key" "webapp_deploy" {
  algorithm = "RSA"
}

resource "aws_key_pair" "public_webapp_key" {
  key_name   = var.deploy_key
  public_key = tls_private_key.webapp_deploy.public_key_openssh
}

// asg


resource "aws_security_group" "vmsap_sg_rules" {
  name = "${var.environment}_vmsap_sg_rules"
  dynamic "ingress" {
    for_each = ["80", "22", "3000", "9090", "9093", "9091", "9100", "8080", "9252", "2376"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Access to vmsap"
  }
}

resource "aws_launch_configuration" "vmsap_lc" {
  name_prefix     = "${var.environment}-vmsap-"
  image_id        = var.ami_id
  instance_type   = var.instance_type
  security_groups = [aws_security_group.vmsap_sg_rules.id]
  user_data       = data.template_file.webapp_user_data.rendered
  key_name        = var.deploy_key

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "vmsap_asg" {
  name                 = "${var.environment}-VMSAP-ASG-${aws_launch_configuration.vmsap_lc.name}"
  launch_configuration = aws_launch_configuration.vmsap_lc.name
  min_size             = var.asg_min_size
  max_size             = var.asg_max_size
  min_elb_capacity     = var.asg_min_elb_capacity
  health_check_type    = "ELB"
   
  vpc_zone_identifier = [aws_default_subnet.vmsap_available_zone_1.id, aws_default_subnet.vmsap_available_zone_2.id]

  dynamic "tag" {
    for_each = {
      #Name        = "${var.environment} Webapp Server in VMSAP-ASG"
      Name        = "Webapp Server in VMSAP-ASG"
      Environment = var.environment
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

// Scale UP and scale DOWN policies for ASG

resource "aws_autoscaling_policy" "vmsap_scale_up" {
  name                   = "${var.environment}-vmsap-scale-up"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.vmsap_asg.name
}

resource "aws_autoscaling_policy" "vmsap_scale_down" {
  name                   = "${var.environment}-vmsap-scale-down"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.vmsap_asg.name
}

// Alarms for CloudWatch 

resource "aws_cloudwatch_metric_alarm" "vmsap_cldwtch_high_cpu" {
  alarm_name          = "cpu-util-high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "80"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.vmsap_asg.name
  }

  alarm_description = "This metric monitors ec2 high cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.vmsap_scale_up.arn]
}

resource "aws_cloudwatch_metric_alarm" "vmsap_cldwtch_low_cpu" {
  alarm_name          = "cpu-util-low"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "30"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.vmsap_asg.name
  }

  alarm_description = "This metric monitors ec2 low cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.vmsap_scale_down.arn]
}

# Application Load Balancer


resource "aws_lb" "vmsap_alb" {
  name               = "${var.environment}-vmsap-alb"
  subnets            = [aws_default_subnet.vmsap_available_zone_1.id, aws_default_subnet.vmsap_available_zone_2.id]
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.vmsap_sg_rules.id]

  tags = {
    Name        = "WebApp VmsAp"
    Environment = var.environment
  }
}

# Attachments to ALB for target groups

resource "aws_autoscaling_attachment" "vmsap_webapp_attachment_lb" {
  autoscaling_group_name = aws_autoscaling_group.vmsap_asg.id
  alb_target_group_arn   = aws_lb_target_group.webapp_vmsap_tg.arn
}

resource "aws_lb_target_group_attachment" "vmsap_monitoring_attachment_lb" {
  target_group_arn = aws_lb_target_group.monitoring_vmsap_tg.arn
  target_id        = aws_instance.vmsap_monitoring.id
}

resource "aws_lb_target_group_attachment" "vmsap_grafana_attachment_lb" {
  target_group_arn = aws_lb_target_group.grafana_vmsap_tg.arn
  target_id        = aws_instance.vmsap_monitoring.id
}

# ALB listeners

resource "aws_lb_listener" "vmsap_webapp_listener" {
  load_balancer_arn = aws_lb.vmsap_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.webapp_vmsap_tg.arn
    type             = "forward"
  }
}

resource "aws_lb_listener_rule" "monitoring_listener_rule" {
  listener_arn = aws_lb_listener.vmsap_webapp_listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.monitoring_vmsap_tg.arn
  }

  condition {
    host_header {
      values = ["${var.prometheus_dns_name}.${var.main_dns_name}"]
    }
  }
}

resource "aws_lb_listener_rule" "grafana_listener_rule" {
  listener_arn = aws_lb_listener.vmsap_webapp_listener.arn
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.grafana_vmsap_tg.arn
  }

  condition {
    host_header {
      values = ["${var.grafana_dns_name}.${var.main_dns_name}"]
    }
  }
}


# ALB target groups

resource "aws_lb_target_group" "webapp_vmsap_tg" {
  name        = "${var.environment}-vmsap-webapp-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_default_subnet.vmsap_available_zone_1.vpc_id
  target_type = "instance"

  health_check {
    interval            = 70
    path                = "/"
    port                = 80
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    protocol            = "HTTP"
    matcher             = "200,202"
  }
}

resource "aws_lb_target_group" "monitoring_vmsap_tg" {
  name        = "${var.environment}-prometheus-webapp-tg"
  port        = 9090
  protocol    = "HTTP"
  vpc_id      = aws_default_subnet.vmsap_available_zone_1.vpc_id
  target_type = "instance"

}

resource "aws_lb_target_group" "grafana_vmsap_tg" {
  name        = "${var.environment}-grafana-webapp-tg"
  port        = 3000
  protocol    = "HTTP"
  vpc_id      = aws_default_subnet.vmsap_available_zone_1.vpc_id
  target_type = "instance"

}

// Roles for monitoring & gitlab-runner servers EC2 read-only

resource "aws_iam_role" "ec2_vmsap_role" {
  name = "${var.environment}_ec2_vmsap_role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY

  tags = {
    Environment = var.environment
  }
}

resource "aws_iam_instance_profile" "ec2_vmsap_profile" {
  name = "${var.environment}_ec2_vmsap_profile"
  role = aws_iam_role.ec2_vmsap_role.name
}

resource "aws_iam_role_policy" "ec2_readonly_policy" {
  name = "${var.environment}_ec2_readonly_policy"
  role = aws_iam_role.ec2_vmsap_role.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "ec2:Describe*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "elasticloadbalancing:Describe*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "cloudwatch:ListMetrics",
                "cloudwatch:GetMetricStatistics",
                "cloudwatch:Describe*"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "autoscaling:Describe*",
            "Resource": "*"
        }
    ]
}
EOF
}



// Gitlab-runner service

resource "aws_instance" "vmsap_gitlab_runner" {
  ami           = var.ami_id
  instance_type = var.instance_type
  key_name      = var.local_key

  vpc_security_group_ids = [aws_security_group.vmsap_sg_rules.id]
  availability_zone      = data.aws_availability_zones.vmsap_available_zones.names[1]
  user_data              = data.template_file.gitlab_runner_user_data.rendered

  iam_instance_profile = aws_iam_instance_profile.ec2_vmsap_profile.name

  provisioner "remote-exec" {
    when = destroy
    inline = [
      "echo 1",
      "sudo docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner unregister --name ${self.tags.Environment}-vmsap-docker-runner",
      "sudo docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner unregister --name ${self.tags.Environment}-vmsap-ansible-runner",
      "echo 2",
    ]

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("/home/aeri/.ssh/id_rsa")
      host        = self.public_ip
    }
  }

  tags = {
    #Name        = "${var.environment} Gitlab-Runner Server"
    Name        = "Gitlab-Runner Server"
    Environment = var.environment
  }

  lifecycle {
    create_before_destroy = true
  }
}

// Monitoring service

resource "aws_instance" "vmsap_monitoring" {
  ami           = var.ami_id
  instance_type = var.instance_type
  key_name      = var.local_key

  vpc_security_group_ids = [aws_security_group.vmsap_sg_rules.id]
  availability_zone      = data.aws_availability_zones.vmsap_available_zones.names[0]
  iam_instance_profile   = aws_iam_instance_profile.ec2_vmsap_profile.name
  user_data              = data.template_file.monitoring_user_data.rendered

  tags = {
    #Name        = "${var.environment} Monitoring Server"
    Name        = "Monitoring Server"
    Environment = var.environment
  }

  lifecycle {
    create_before_destroy = true
  }
}

// Route53 service

data "aws_route53_zone" "vmsap_hosted_zone" {
  name         = "${var.main_dns_name}."
  private_zone = false
}

resource "aws_route53_record" "test-vmsap-tk" {
  zone_id = data.aws_route53_zone.vmsap_hosted_zone.zone_id

  name = (var.environment == "test" ? "${var.webapp_dns_name}.${data.aws_route53_zone.vmsap_hosted_zone.name}" : "${data.aws_route53_zone.vmsap_hosted_zone.name}")

  type = "A"

  alias {
    name                   = aws_lb.vmsap_alb.dns_name
    zone_id                = aws_lb.vmsap_alb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "monitoring-vmsap-tk" {
  zone_id = data.aws_route53_zone.vmsap_hosted_zone.zone_id
  name    = "${var.prometheus_dns_name}.${data.aws_route53_zone.vmsap_hosted_zone.name}"
  type    = "A"

  alias {
    name                   = aws_lb.vmsap_alb.dns_name
    zone_id                = aws_lb.vmsap_alb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "grafana-vmsap-tk" {
  zone_id = data.aws_route53_zone.vmsap_hosted_zone.zone_id
  name    = "${var.grafana_dns_name}.${data.aws_route53_zone.vmsap_hosted_zone.name}"
  type    = "A"

  alias {
    name                   = aws_lb.vmsap_alb.dns_name
    zone_id                = aws_lb.vmsap_alb.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "docker-vmsap-tk" {
  zone_id = data.aws_route53_zone.vmsap_hosted_zone.zone_id
  name    = "${var.docker_dns_name}.${data.aws_route53_zone.vmsap_hosted_zone.name}"
  ttl     = 172800
  type    = "A"
  records = [aws_instance.vmsap_gitlab_runner.public_ip]  
}
