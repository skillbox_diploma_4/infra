#!/bin/bash

echo "${publickey}" > /home/ubuntu/.ssh/publickey
echo "${privatekey}" > /home/ubuntu/.ssh/privatekey.pem
sudo chmod 0600 /home/ubuntu/.ssh/privatekey.pem

curl -O https://gitlab.com/skillbox_diploma_4/infra/-/raw/main/ansible/gtlb_config/docker-compose.yml
docker-compose up -d
