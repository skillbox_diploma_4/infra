#!/bin/bash

curl -O https://gitlab.com/skillbox_diploma_4/infra/-/archive/main/infra-main.zip && \
unzip infra-main.zip "infra-main/ansible/monitoring_config/*" && \
sudo cp -r infra-main/ansible/monitoring_config/dockprom/ /home/ubuntu/dockprom/ && \
cd /home/ubuntu/dockprom

docker-compose --env-file .env.${srv_env} up