#!/bin/bash
sudo apt update -y
sudo apt install nginx -y
sudo systemctl restart nginx

if [ ${srv_env} == "test" ]; then brnch="uat"; elif [ ${srv_env} == "prod" ]; then brnch="main"; fi

curl -X POST -F token=b26658d9aaed6421266a405592eb27 \
-F ref=$brnch -F "variables[TRIGGERED]=yes" \
-F "variables[SRV_ENV]=${srv_env}" \
https://gitlab.com/api/v4/projects/29446444/trigger/pipeline

curl -O https://gitlab.com/skillbox_diploma_4/infra/-/raw/main/ansible/webapp_config/docker-compose.yml
docker-compose up -d