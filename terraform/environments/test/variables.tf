# variables for project
# values for variables are in test.tfvars for test environment
# values for variables are in prod.tfvars for prod environment

#  key for webapp's deploy 
variable "deploy_key" {}

# key for managing gitlab-runner's server
variable "local_key" {
  type    = string
  default = "ubnt20-home"
}

variable "region" {}
variable "environment" {}

# vars for instances
variable "ami_id" {}
variable "instance_type" {}

# vars for autoscaling group
variable "asg_min_size" {}
variable "asg_max_size" {}
variable "asg_min_elb_capacity" {}

# vars for dns names
# must be registered in Router53 service before use

# hosted zone 
variable "main_dns_name" {}

# prefix for endpoints (dns name 3 level)
variable "webapp_dns_name" {}
variable "prometheus_dns_name" {}
variable "grafana_dns_name" {}
variable "docker_dns_name" {}




